const createChessBoard = (boards: number) => {
    let board = "";
    for (let i = 1; i < boards*boards; i += 1) {
        if ((i % (boards + 1)) == 0) {
            board += "\n";
        } else if (i % 2 != 0) {
            board += "  ";
        } else {
            board += "██";
        }
    }

    return board;
}

console.log(createChessBoard(8));